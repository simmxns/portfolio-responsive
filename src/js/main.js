const showMenu = (toggleId, navId) => {
	const toggle = document.getElementById(toggleId),
		nav = document.getElementById(navId)

	if (toggle && nav) {
		toggle.addEventListener('click', () => {
			nav.classList.toggle('show')
		})
	}
}
showMenu('nav-toggle', 'nav-menu')

const navLink = document.querySelectorAll('.nav__link')

function linkAction() {
	navLink.forEach(n => n.classList.remove('active'))
	this.classList.add('active')

	const navMenu = document.getElementById('nav-menu')
	navMenu.classList.remove('show')
}

navLink.forEach(n => n.addEventListener('click', linkAction))

//scroll reveal
const sr = ScrollReveal({
	origin: 'top',
	distance: '80px',
	duration: 2000,
	reset: true,
})

sr.reveal('.home__title', {})
sr.reveal('.button', { delay: 200 })
sr.reveal('.home__img', { delay: 400 })
sr.reveal('.home__social-icon', { interval: 200 })

sr.reveal('.about__img', {})
sr.reveal('.about__subtitle', { delay: 200 })
sr.reveal('.about__text', { delay: 400 })

sr.reveal('.skills__subtitle', {})
sr.reveal('.skills__text', { delay: 200 })
sr.reveal('.skills__data', { interval: 200 })
sr.reveal('.skills__img', { delay: 400 })

sr.reveal('.work__img', { interval: 200 })

sr.reveal('.contact__input', { interval: 200 })

//dark mode
const changeTheme = toggleSwitch => {
	const toggle = document.getElementById(toggleSwitch)

	const secondColor = '--second-color'
	const whiteDm = '--white-dm'
	const bgColor = '--background-color'
	const grayColor = '--gray-color'
	const darkblueColor = 'darkblue-color'

	toggle.addEventListener('click', () => {
		if (toggle.checked) {
			document.documentElement.style.setProperty(secondColor, '#e1e1e1')
			document.documentElement.style.setProperty(whiteDm, '#181a26')
			document.documentElement.style.setProperty(bgColor, '#181a26')
			document.documentElement.style.setProperty(grayColor, 'rgba(138, 140, 158, 0.15)')
			document.documentElement.style.setProperty(darkblueColor, 'rgba(138, 140, 158, 0.08)')
		} else {
			document.documentElement.style.setProperty(secondColor, '#22232e')
			document.documentElement.style.setProperty(whiteDm, '#fff')
			document.documentElement.style.setProperty(bgColor, '#fff')
			document.documentElement.style.setProperty(grayColor, 'rgba(0, 0, 0, 0.15)')
			document.documentElement.style.setProperty(darkblueColor, 'rgba(0, 0, 0, 0.15)')
		}
	})
}
changeTheme('switch')
