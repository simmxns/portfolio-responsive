# Portfolio Responsive 🙋‍♂️
Simple minimal website with dark mode and scroll reveal

## Demo
<div align="center">
  <img src="https://github.com/sssimxn/portfolio__responsive/blob/main/demo.gif" />
</div><br>

<p><em>30fps btw :c</em></p>

## Tech
* HTML, CSS, JS
* [Box icons](https://boxicons.com/)
* [Scroll Reveal](https://scrollrevealjs.org/)

## Wiki
[Switch (Checkbox)](https://github.com/sssimxn/portfolio__responsive/wiki/Switch-(Checkbox))
